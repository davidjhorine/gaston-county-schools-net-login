# Import some required stuff
import json
from os import path
from pathlib import Path
import requests

# Get this variable ready just in case
troubleshooting = """
Failed to login! Here's some things to try:
1: Disconnect and reconnect to the wifi network
2: Delete the config file and rerun the program to generate a new one
3: Reboot your computer
            """

# Find the home directory to store the config file
home = str(Path.home())

# //Code start!//
print("GCS Netlogin v2 - Developed for David Horine for use with Gaston County Schools")

# Check to see if config file exists
if path.exists(home + "/gcs_netlogin_config.json"):
    print("Found a config file! Now loading from " + home + "/gcs_netlogin_config.json")
    # Load the config file to the config variable
    with open(home + "/gcs_netlogin_config.json") as f:
        config = json.load(f)
else:
    # Initialize the config dictionary
    config = {}
    print("No config file exists! Starting to create one")
    config["student_id"] = input("What is your student id? ") # Get SID
    config["password"] = input("What is your password? ") # Get PW
    # Save the config file
    with open(home + "/gcs_netlogin_config.json", "w") as f:
        json.dump(config, f)
    print("Created the config as " + home + "/gcs_netlogin_config.json, to update your configuration, delete this file")
    print("Rerun this file to log in to the network.")
    input("Press enter to continue")
    exit()
    
# Set the data required to submit the form
data = {"network_name" : "Guest Network", "buttonClicked" : "4", "err_flag" : "0", "err_msg" : "", "redirect_url" : "http://example.com" , "info_flag" : "0" , "info_msg" : ""}
data["username"] = config["student_id"]
data["password"] = config["password"]

# Set the content type header
headers = {"Content-Type" : "application/x-www-form-urlencoded"}

# Finally, make the actual request
login = requests.post("https://1.1.1.1/login.html" , headers = headers , data = data , verify = False)
try:
    if "Descriptions in words" in str(requests.get("https://www.w3.org/TR/PNG/iso_8859-1.txt").content):
        print("Successfully logged in!")
        input("Press enter to continue")
    else:
        print(troubleshooting)
        input("Press enter to continue")
except:
    print(troubleshooting)
    input("Press enter to continue")
    
