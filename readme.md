# GCS Netlogin
## Quickly log in to Gaston County Schools BYOD networks

After many unreliable fights with a login page that never wants to load, endless "sign in to this network" loops, and getting signed out every time I changed locations, I decided to create a better solution.

## Downloads

You can get a zip file containing all the files you'll need at the [releases](https://gitlab.com/electronicbrain0704/gaston-county-schools-net-login/-/releases) page.

## How to use on Windows

Move gcs_netlogin_windows.exe to a location of your choice, then double click the file to run. If you'd like, you can drag the file to your task bar for quicker access.

## How to use on Mac OS

Sorry I can't provide more detailed instructions right now! I'll try to update this later, but for now, place "gcs_netlogin_mac" on your desktop, open the "Terminal" app, type `~/Desktop/gcs_netlogin_mac`, and hit enter.

## How to use on linux

Open a terminal in the directory that you downloaded this folder and type the following command to install GCS Netlogin system-wide:

`sudo cp gcs_netlogin_linux /usr/bin/gcs_netlogin`

Once this is done, type `gcs_netlogin` in the terminal to configure the program. Once configured, press \[Alt] and \[F2] together, then type `gcs_netlogin` to log in to the WiFi network.

## Other platforms and technical information

If you're running on another platform, try to find a python interpreter for your platform of choice and use it to run "gcs_netlogin_python.py". The script was written for Python 3.6, but is confirmed to work on Python 3.8 and will likely work on most versions of Python 3.

The individual platform executables were compiled with pyinstaller using the -F flag to generate a single executable.

## Disclaimer

I *don't think* this software will get you banned from the network or raise any flags. That being said, this software makes no attempt to mask itself and pretend it is a web browser. Gaston County Schools IT administrators could very easily see that this is an automated script. 

I **am not** responsible if you get banned. I take no liability for anything that may happen to your ability to access the network due to use or abuse of this script.